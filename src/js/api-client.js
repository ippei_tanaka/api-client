var Trapeze = Trapeze || {};

(function (NAMESPACE, _, $) {
    var ApiClient = function (config) {
        config = config ? config : {};
        this.filters = {};
        this.previousFilters = {};
        this.host = config.host || "localhost";
        this.port = config.port || "";
        this.rootDir = config.rootDir || "/";
        this.dataType = config.dataType || "json";
        this.urlFactory = config.urlFactory || this.defaultURLFactory;
        this.separators = {
            betweenKeyAndValue: "=",
            betweenValues: ",",
            betweenParams: "&"
        };
        config.separators ? $.extend(true, this.separators, config.separators) : "";

        this.result = null;
        this.requestCounter = 0;
    };

    ApiClient.prototype.addFilter = function () {
        var filters;
        if (arguments.length === 1 &&
            _(arguments[0]).isObject()) {
            filters = arguments[0];
        } else if (arguments.length === 2 &&
            _(arguments[0]).isString()) {
            filters = {};
            filters[arguments[0]] = arguments[1];
        } else {
            throw new TypeError();
        }
        _(filters).each(_(function (filter, index) {
            this.filters[index] = filter;
        }).bind(this));
    };

    ApiClient.prototype.removeFilter = function (key) {
        if (!_(key).isString() || !_(key).isNumber()) {
            throw new TypeError();
        }
        delete this.filters[key];
    };

    ApiClient.prototype.getFilters = function () {
        return this.filters;
    };

    ApiClient.prototype.getResult = function () {
        return this.result;
    };

    ApiClient.prototype.getRequestCounter = function () {
        return this.requestCounter;
    };

    ApiClient.prototype.applyFilters = function () {
        var deferred = $.Deferred();
        if (_(this.filters).isEqual(this.previousFilters)) {
            deferred.resolve();
            return deferred.promise();
        } else {
            return this.getResource();
        }
    };

    ApiClient.prototype.getResource = function () {
        var ajaxConfig = {
            url: this.createURL(),
            dataType: this.dataType
        };
        return $.ajax(ajaxConfig)
            .pipe(_(function (data) {
                this.previousFilters = _.clone(this.filters);
                this.result = data;
            }).bind(this))
            .always(_(function () {
                this.requestCounter += 1;
            }).bind(this));
    };

    ApiClient.prototype.createURL = function () {
        return this.urlFactory.apply(this,
            [
                this.createBaseURL(this.host, this.port, this.rootDir),
                this.filters,
                this.host,
                this.port,
                this.rootDir
            ]);
    };

    ApiClient.prototype.createBaseURL = function (host, port, rootDir) {
        return "//" + host + (port ? ":" + port : "") + rootDir;
    };

    ApiClient.prototype.defaultURLFactory = function (baseURL, filters, host, port, rootDir) {
        return baseURL + this.createURLParams(filters);
    };

    ApiClient.prototype.createURLParams = function (filters) {
        return (_(filters).size() > 0 ? "?" + this.serialize(filters) : "");
    };

    ApiClient.prototype.serialize = function (object) {
        var tempArray = [];
        _(object).each(_(function (value, key) {
            if (_(value).isArray()) {
                tempArray.push(key + this.separators.betweenKeyAndValue + value.join(this.separators.betweenValues));
            } else {
                tempArray.push(key + this.separators.betweenKeyAndValue + value);
            }
        }).bind(this));
        return tempArray.join(this.separators.betweenParams);
    };

    NAMESPACE.ApiClient = ApiClient;
})(Trapeze, _, jQuery);