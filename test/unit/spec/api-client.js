describe('ApiClient', function () {

    var filters = null;

    beforeEach(function () {
        filters = new Trapeze.ApiClient();
    });

    describe('addFilters()', function () {
        it('should throw TypeError when adding invalid values', function () {
            expect(function () {
                filters.addFilter({bar: 5}, {foo: 4});
            }).to.throw(TypeError);
            expect(function () {
                filters.addFilter("bar", 4, 5);
            }).to.throw(TypeError);
        });
    });

    describe('removeFilters()', function () {
        it('should throw TypeError when adding invalid values', function () {
            expect(function () {
                filters.removeFilter({});
            }).to.throw(TypeError);
        });
        it('should remove the filter', function () {
            expect(function () {
                filters.addFilter("bar", 1);
                filters.addFilter("foo", 2);
                filters.removeFilter("noname");
                filters.removeFilter("bar");
                expect(filters.getFilters()).to.deep.equal({
                    foo: 2
                });
            }).to.throw(TypeError);
        });
    });

    describe('getFilters()', function () {
        it('should return filter object when adding filters', function () {
            filters.addFilter("test1", 1);
            filters.addFilter("test2", 2);
            filters.addFilter({test3: 3});
            expect(filters.getFilters()).to.deep.equal({
                test1: 1,
                test2: 2,
                test3: 3
            });
        });
        it('should return filter object when updating filters', function () {
            filters.addFilter({
                foo: 1,
                bar: 2
            });
            filters.addFilter({
                foo: 3,
                foobar: 4
            });
            filters.addFilter("foobar", 5);
            expect(filters.getFilters()).to.deep.equal({
                foo: 3,
                bar: 2,
                foobar: 5
            });
        });
    });

    describe('serialize()', function () {
        it('should return filter param string after adding filters', function () {
            expect(filters.serialize({
                a: 1,
                b: [2, 3]
            })).to.equal("a=1&b=2,3");
            expect(filters.serialize({
                a: 1,
                b: [2, 3],
                c: [4, 5, {d: 6, e: [7, 8]}]
            })).to.equal("a=1&b=2,3&c=4,5,[object Object]");
        });
    });

    describe('applyFilters()', function () {
        it('should update result', function (done) {
            var filters = new Trapeze.ApiClient({
                host: "www1.shoppersdrugmart.ca",
                rootDir: "/en/store/GetStores"
            });
            filters.addFilter({
                latitude: 43.653226,
                longitude: -79.38318429999998
            });
            expect(filters.getResult()).to.equal(null);
            filters.applyFilters().done(function () {
                expect(filters.getResult().count).to.equal(38);
                done();
            });
        });
        it('should not request new ajax request when filters are not updated', function (done) {
            var filters = new Trapeze.ApiClient({
                host: "www1.shoppersdrugmart.ca",
                rootDir: "/en/store/GetStores"
            });
            filters.addFilter({
                latitude: 43.653226,
                longitude: -79.38318429999998
            });
            expect(filters.getRequestCounter()).to.equal(0);
            filters.applyFilters().done(function () {
                expect(filters.getRequestCounter()).to.equal(1);
                filters.applyFilters().done(function () {
                    expect(filters.getRequestCounter()).to.equal(1);
                    filters.addFilter("foo", 123);
                    filters.applyFilters().done(function () {
                        expect(filters.getRequestCounter()).to.equal(2);
                        done();
                    });
                });
            });
        });
    });

    describe('createURL', function () {
        it('should change the way to create URL when it configures urlFactory', function () {
            var filters = new Trapeze.ApiClient({
                host: "test.com",
                port: 8080,
                rootDir: "/api",
                urlFactory: function (baseURL, filters, host, port, rootDir) {
                    return rootDir + "/user/" + filters.userId + "/" + filters.search;
                }
            });
            filters.addFilter({
                userId: 2,
                search: "?gender=male"
            });
            expect(filters.createURL()).to.equal("/api/user/2/?gender=male");
        });
    });
});